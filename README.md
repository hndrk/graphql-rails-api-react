## Tutorial files

This code was created following the tutorial [GraphQL Subscriptions with a Standalone Rails API and React](https://haughtcodeworks.com/blog/software-development/graphql-rails-react-standalone).

---

## To run this app

1. Make sure to include your own database.yml to allow database connections.
2. Go to app folder, then run `rails s` to execute puma.
3. Go to the client folder (graphql-react-client) and run `npm start`.

---

## Changes

In order to get things running, the following changes had to be made to the original code

1. Replace **localhost** with VM IP address (both in client and api)
2. Allow different [request origin](https://stackoverflow.com/questions/35188892/request-origin-not-allowed-http-localhost3001-when-using-rails5-and-actionca)
3. Solve [FATAL: Listen error: unable to monitor directories for changes](https://stackoverflow.com/questions/42225677/listen-error-unable-to-monitor-directories-for-changes)